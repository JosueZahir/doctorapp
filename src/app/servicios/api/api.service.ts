import { Injectable } from '@angular/core';
import {LoginI} from '../../modelos/login.interface';
import {ResponseI} from '../../modelos/response.interface';
import {ListaI} from '../../modelos/lista.interface';
import {PacienteI} from '../../modelos/paciente.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url : string = "http://apsmjl.aplicacionesmoviles.com.mx/log/";
  constructor(private http:HttpClient) { }

  //el observable es para saber que tipo de dato va a retornar responseI
  //post
  loginEmail(form:LoginI):Observable<ResponseI>{
    let direccion = this.url + "auth.php";
    return this.http.post<ResponseI>(direccion,form);
  }

  getAll(page:number):Observable<ListaI[]>{
    let direccion = this.url + "pacientes.php?page=" + page;
    return this.http.get<ListaI[]>(direccion); 
  }

  getSP(id):Observable<PacienteI>{
    let direccion = this.url + 'pacientes.php?id=' + id;
    return this.http.get<PacienteI>(direccion);
  }

  putPaciente(form:PacienteI):Observable<ResponseI>{
    let direccion = this.url + 'pacientes.php';
    return this.http.put<ResponseI>(direccion, form);
  }

  delatePaciente(form:PacienteI):Observable<ResponseI>{
    let direccion = this.url + 'pacientes.php';
    let Options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body:form
    }
    return this.http.delete<ResponseI>(direccion, Options);
  }
  

  postPaciente(form:PacienteI):Observable<ResponseI>{
    let direccion=this.url + 'pacientes.php';
    return this.http.post<ResponseI>(direccion, form);
  }
  
}
