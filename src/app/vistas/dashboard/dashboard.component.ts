import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service';
import {Router} from '@angular/router';
import {ListaI} from '../../modelos/lista.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pacientes:ListaI[];

  constructor(private api:ApiService, private rt:Router) { }

  ngOnInit(): void {
    this.api.getAll(1).subscribe(data =>{
      //console.log(data);
      this.pacientes = data;
    });
  }

  EditarP(id){
    console.log(id);
    this.rt.navigate(['editar',id]);
  }

  NuevoP(){
    this.rt.navigate(['nuevo']);
  }
}
