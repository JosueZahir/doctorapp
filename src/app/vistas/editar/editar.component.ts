
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {PacienteI} from '../../modelos/paciente.interface';
import {ApiService} from '../../servicios/api/api.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ResponseI} from '../../modelos/response.interface';
import {AlertasService} from '../../servicios/alertas/alertas.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  constructor(private Arouter:ActivatedRoute, private rt:Router, private api:ApiService, private alertas:AlertasService) { }

  datosP:PacienteI;
  editarF = new FormGroup({
    nombre: new FormControl(''),
    correo: new FormControl(''),
    dni: new FormControl(''),
    direccion: new FormControl(''),
    codigoPostal: new FormControl(''),
    genero: new FormControl(''),
    telefono: new FormControl(''),
    fechaNacimiento: new FormControl(''),
    token: new FormControl(''),
    pacienteId: new FormControl('')
  });

  ngOnInit(): void {
    let pacienteId = this.Arouter.snapshot.paramMap.get('id');
    //console.log(pacienteId);
    let token = this.getToken();
    //console.log(token);
    this.api.getSP(pacienteId).subscribe(data => {
      this.datosP = data[0];
      //console.log(this.datosP);
      this.editarF.setValue({
        'nombre' : this.datosP.Nombre,
        'correo' : this.datosP.Correo,
        'dni' : this.datosP.DNI,
        'direccion' : this.datosP.Direccion,
        'codigoPostal' : this.datosP.CodigoPostal,
        'genero' : this.datosP.Genero,
        'telefono' : this.datosP.Telefono,
        'fechaNacimiento' : this.datosP.FechaNacimiento,
        'token' : token,
        'pacienteId' : pacienteId
      });
      //console.log(this.editarF.value);
    });
  }

  getToken(){
    return localStorage.getItem('token');
  }

  postForm(form:PacienteI){
    this.api.putPaciente(form).subscribe(data=>{
      let respuesta:ResponseI = data;
      if(respuesta.status=="ok"){
        this.alertas.showSuccess("Datos Modificados", "Hecho");
        this.rt.navigate(['dashboard']);
      }else{
        this.alertas.showError(respuesta.result.error_msg,"Error");
      }
    });
    //console.log(form);
  }
  eliminar(){
    //console.log("eliminar");
    let datos:PacienteI = this.editarF.value;
    this.api.delatePaciente(datos).subscribe(data=>{
      let respuesta:ResponseI = data;
      if(respuesta.status=="ok"){
        this.alertas.showSuccess("Datos Eliminados", "Hecho");
      }else{
        this.alertas.showError(respuesta.result.error_msg,"Error");
      }
    });
  }
  salir(){
    this.rt.navigate(['dashboard']);
  }

}
